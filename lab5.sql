--1
use ITI

create proc show_st_num as
select Department.Dept_Name,count(St_Id) as [number of students] from Student,Department
where Student.Dept_Id = Department.Dept_Id
group by Department.Dept_Name

--2
use Company_SD

create proc morethan_three as
declare @stnum int
set @stnum = (select COUNT(*) from Works_for where Pno = 100)

if @stnum > 3
select 'The number of employees in the project p1 is 3 or more' as [OUTPUT]

if @stnum < 3
declare @names table(fullname varchar(30))
insert into  @names select CONCAT(Fname,' ',Lname) from Employee where  
SSN in (select Works_for.ESSn from Works_for where Pno = 100)

select concat
('The following employees work for the project p1',
(select fullname from (select ROW_NUMBER()  OVER(ORDER BY fullname) as rnum ,fullname from @names ) t  where rnum = 1  ),' and ',
(select fullname from (select ROW_NUMBER()  OVER(ORDER BY fullname) as rnum ,fullname from @names ) t  where rnum = 2  ))  as [OUTPUT]
 
 morethan_three

 --3

 create proc newemp @oldempnum int,@newempnum int,@pjnum int
 as
 update Works_for 
 set ESSn = @newempnum 
 where ESSn = @oldempnum and Pno = @pjnum
 
 newemp 112233,6969,100

 --4
 alter table project add Budget int 
 create table pjAudit 
 (ProjectNo int,
 UserName varchar(30),
 ModifiedDate date,
 Budget_Old int,
 Budget_New int 
) 
use Company_SD

create trigger pjediting 
on Project
after update as
if (select Pnumber from inserted) is not null
insert into  pjAudit values(
(select Pnumber from inserted),
SUSER_NAME(),
getdate(),
(select Budget from deleted),
(select Budget from inserted)
)

update Project 
set Budget = 69000
where Pnumber =100

--5
use ITI

create trigger SAFTY_INS
on Department
instead OF INSERT 
AS
SELECT ' You can�t insert a new record in that table' as [ERROR] 

insert into Department(Dept_Id) values(55)

--6
use Company_SD

create trigger march_safety
on Employee
instead OF INSERT 
as
 if  (select MONTH( GETDATE())) = 3
SELECT ' You can�t insert a new record in  March ' as [ERROR] 

else
insert into Employee select * from inserted 

insert into Employee(SSN) values(55)

--7
use ITI

create trigger stediting 
on student
after update as

if (select St_Id from inserted) is not null
insert into  STAuditing values(
(
SUSER_NAME(),
getdate(),
concat(SUSER_NAME() ,'Insert New Row with Key=',
(select St_Id from inserted),' in table ','Student')
)

--8
create trigger no_st_delete
on Student 
instead of delete 
as
if (select St_Id from inserted) is not null
insert into  STAuditing values(
(
SUSER_NAME(),
getdate(),
concat(SUSER_NAME() ,'try to delete Row with Key',
(select St_Id from inserted),' in table ','Student')
)


--9
use AdventureWorks2012

select * from HumanResources.Employee
for xml raw('Employee'),ELEMENTS,ROOT('EmployeeS')

select * from HumanResources.Employee
for xml raw('Employee'),ROOT('EmployeeS')

--10
use ITI
select  Department.Dept_Name "Name",
	   Department.Dept_Manager "Manger"
from Department
for xml path('Department'),root('Departments')

select  *
from Department
for xml auto,root('Departments')


--11
use Company_SD

declare @docs xml ='<customers> 

              <customer FirstName="Bob" Zipcode="91126"> 

                     <order ID="12221">Laptop</order> 

              </customer> 

              <customer FirstName="Judy" Zipcode="23235"> 

                     <order ID="12221">Workstation</order> 

              </customer> 

              <customer FirstName="Howard" Zipcode="20009"> 

                     <order ID="3331122">Laptop</order> 

              </customer> 

              <customer FirstName="Mary" Zipcode="12345"> 

                     <order ID="555555">Server</order> 

              </customer> 

       </customers>' 

	   select*
  FROM OPENXML (@docs,'//customer')  --levels  XPATH Code
WITH (FirstName varchar(10) '@StudentID',
	  Zipcode int '@Zipcode', 
	  [Order] varchar(10) '/order',
	  [Oder ID] int '//@ID'
	  )

	  Exec sp_xml_removedocument @docs
