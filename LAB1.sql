use iti;

--1
select COUNT(*) from Student
where St_Age is not null

--2
select DISTINCT Ins_Name  from Instructor

--3
select St_Id as [Student ID] ,
CONCAT(isnull(st_fname,'MR.'),' ',St_Lname) as [Student Full Name],
Dept_Name as [Department name]
from Student,Department 
where Student.Dept_Id = Department.Dept_Id

--4
select Ins_Name,Dept_Name from Instructor as i,Department as d
where i.Dept_Id = d.Dept_Id

--5
select
CONCAT(isnull(st_fname,'MR.'),' ',St_Lname) as [Student Full Name],
n.Crs_Name as [Course Name]
from Student as s,Stud_Course as c,Course n
where c.St_Id = s.St_Id and c.Crs_Id = n.Crs_Id and c.Grade is not null

--6
select Top_Name,count(Course.Top_Id) as [number of courses]
from Topic,Course
where Topic.Top_Id = Course.Top_Id
group by Top_Name

--7
select *
from (select *, Row_number() over(order by Salary desc) as RN
	  from Instructor) as newtable
where Rn=1  or RN = (select COUNT(*) FROM Instructor)

--8
select * from Instructor
where Salary <  (select AVG(Salary) from Instructor)


--9
select Dept_Name from Department,Instructor
where Instructor.Dept_Id = Department.Dept_Id 
and Salary = (select MIN(Salary) from Instructor)

--10
select top(2)*
from (select *, Dense_rank() over(order by Salary desc) as RN
	  from Instructor) as newtable
where Rn=1 


--11 
select Ins_Name, coalesce(cast(Salary as varchar),'BONUS')
from Instructor;

--12
select AVG(Salary)	from Instructor

--13
select St_Fname,Instructor.* from Student,Instructor
where Student.St_super = Instructor.Ins_Id

--14
select *
from (select *, Row_number() over(order by Salary desc) as RN
	  from Instructor) as newtable
where Rn=1  or RN = 2
--15
select *
from (select *, row_number() over(order by newid()) as RN
	  from Instructor) as newtable
where RN=1 




--PART (2)

use AdventureWorks2012

--1
select SalesOrderID, ShipDate from Sales.SalesOrderHeader 
where OrderDate > '7/28/2002'   and OrderDate < '7/29/2014'

--2
select Name,ProductID from Production.Product 
where StandardCost <110.00

--3
select Name,ProductID from Production.Product 
where Weight is null

--4
select * from Production.Product 
where Color = 'Silver' or Color =  'Black' or Color = 'Red'

--5
select * from Production.Product 
where Name like 'B%'

--6
UPDATE Production.ProductDescription 
SET Description = 'Chromoly steel_High of defects' 
WHERE ProductDescriptionID = 3 

select Description from Production.ProductDescription 
where Description like '%[_]%'

--7
select OrderDate,SUM(TotalDue) from Sales.SalesOrderHeader
where OrderDate > '7/1/2001'   and OrderDate < '7/31/2014'
group by OrderDate

--8
select distinct HireDate  from HumanResources.Employee

--9
select  AVG(ListPrice) from Production.Product
group by ListPrice

--10
select CONCAT('The ',Name,' is only! ',ListPrice) from Production.Product
where ListPrice between 100 and 120
order by ListPrice asc

--11
 --a
select rowguid,Name,SalesPersonID,Demographics  into  store_Archive 
FROM Sales.Store
--b
select rowguid,Name,SalesPersonID,Demographics  into  store_Archive2 
FROM Sales.Store where 1=2

--12

select CONVERT(varchar, getdate(), 1) as [dd/mm/yy]
union
select CONVERT(varchar, getdate(), 2) as [dd/mm/yy]
union
select CONVERT(varchar, getdate(), 3) as [dd/mm/yy]
union
select CONVERT(varchar, getdate(), 4) as [dd/mm/yy]
union
select CONVERT(varchar, getdate(), 5) as [yy/mm/dd]
union
select CONVERT(varchar, getdate(), 6) as [yy/m/dd]



